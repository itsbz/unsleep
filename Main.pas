unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ExtCtrlsX, ImgList, Buttons;

type
  TForm1 = class(TForm)
    CursorTimer: TTimer;
    AltTabTimer: TTimer;
    TrayIcon1: TTrayIcon;
    ImageList1: TImageList;
    Button1: TSpeedButton;
    SpeedButton1: TSpeedButton;
    WorkTimer: TTimer;
    Label1: TLabel;
    WorkLabel: TLabel;
    RestLabel: TLabel;
    Label4: TLabel;
    RestTimer: TTimer;
    WorkTimeLabel: TLabel;
    RestTimeLabel: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure CursorTimerTimer(Sender: TObject);
    procedure AltTabTimerTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
    procedure TrayIcon1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure WorkTimerTimer(Sender: TObject);
    procedure RestTimerTimer(Sender: TObject);
  private { Private declarations }
  public { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if (CursorTimer.Enabled) then
  begin
    CursorTimer.Enabled := False;
    AltTabTimer.Enabled := False;
    Button1.Caption := 'Paused';
    TrayIcon1.IconIndex := 1;
    WorkTimer.Enabled := False;
    RestTimer.Enabled := False;
  end
  else
  begin
    CursorTimer.Enabled := True;
    AltTabTimer.Enabled := True;
    Button1.Caption := 'Started';
    TrayIcon1.IconIndex := 0;
    WorkTimer.Enabled := True;
    RestTimer.Enabled := False;
  end
end;

procedure TForm1.CursorTimerTimer(Sender: TObject);
var
  pos, newPos: TPoint;
begin
  newPos.X := Random(Screen.Width);
  newPos.Y := Random(Screen.Height);
  GetCursorPos(pos);
  while ((newPos.X <> pos.X) and (newPos.Y <> pos.Y)) do
  begin
    if (pos.X < newPos.X) then
      pos.X := pos.X + Random(5)
    else
      pos.X := pos.X - Random(5);
    if (pos.Y < newPos.Y) then
      pos.Y := pos.Y + Random(5)
    else
      pos.Y := pos.Y - Random(5);

    SetCursorPos(pos.X, pos.Y);
    if (Random(100) > 90) then
      mouse_event(MOUSEEVENTF_WHEEL, 0, 0, DWORD(-WHEEL_DELTA + Random(3 *
        WHEEL_DELTA)), 0);
    Application.ProcessMessages;
    Sleep(5);
  end
end;

procedure Keybd_Press(const AKey: byte);
begin
  Keybd_Event(AKey, 0, 0, 0);
end;

procedure Keybd_Release(const AKey: byte);
begin
  Keybd_Event(AKey, 0, KEYEVENTF_KEYUP, 0);
end;

procedure TForm1.AltTabTimerTimer(Sender: TObject);
var
  f, i: Integer;
begin
  Randomize;
  f := Random(100);
  Caption := 'UnSleep ' + IntToStr(f);
  if (f < 80) then
    Exit;

  Keybd_Press(VK_MENU);
  for i := 1 to Random(10) do
  begin
    Keybd_Press(VK_TAB);
    Keybd_Release(VK_TAB);
  end;
  Keybd_Release(VK_MENU);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caNone;
  Form1.Hide;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.TrayIcon1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then
    Button1.Click
  else
    Form1.Show;
end;

procedure TForm1.WorkTimerTimer(Sender: TObject);
var
  workTime, restTime: Integer;
begin
  workTime := StrToInt(WorkLabel.Caption);
  workTime := workTime - 1;
  WorkLabel.Caption := IntToStr(workTime);
  WorkTimeLabel.Caption := IntToStr(workTime div 60) + ':' + IntToStr(workTime mod 60);
  WorkTimeLabel.Font.Color := clRed;
  RestTimeLabel.Font.Color := clBlack;

  if (workTime <= 0) then
  begin
    WorkTimer.Enabled := False;
    WorkLabel.Caption := IntToStr(60*40 + Random(60*20));
    AltTabTimer.Enabled := False;
    CursorTimer.Enabled := False;
    WorkTimeLabel.Font.Color := clBlack;

    RestLabel.Caption := IntToStr(60*10 + Random(60*20));
    restTime := StrToInt(RestLabel.Caption);
    RestTimeLabel.Caption := IntToStr(restTime div 60) + ':' + IntToStr(restTime mod 60);
    RestTimer.Enabled := True;
    RestTimeLabel.Font.Color := clRed;
  end;
end;

procedure TForm1.RestTimerTimer(Sender: TObject);
var
  restTime, workTime: Integer;
begin
  restTime := StrToInt(RestLabel.Caption);
  restTime := restTime - 1;
  RestLabel.Caption := IntToStr(restTime);
  RestTimeLabel.Caption := IntToStr(restTime div 60) + ':' + IntToStr(restTime mod 60);
  if (restTime <= 0) then
  begin
    RestTimer.Enabled := False;
    RestLabel.Caption := IntToStr(60*40 + Random(60*20));

    WorkLabel.Caption := IntToStr(60*10 + Random(60*20));
    workTime := StrToInt(WorkLabel.Caption);
    WorkTimeLabel.Caption := IntToStr(workTime div 60) + ':' + IntToStr(workTime mod 60);
    WorkTimer.Enabled := True;
    AltTabTimer.Enabled := True;
    CursorTimer.Enabled := True;
  end;
end;

end.

